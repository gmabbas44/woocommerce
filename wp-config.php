<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'woocommerce' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'ZCjjIc?j{7I|DKfxgZlbFJEPnKNGc8&su]=%ZVtNU[!(oq?m:rRAKZ{|fEi. )Oz' );
define( 'SECURE_AUTH_KEY',  'u]-JE=#&i^?b#?B9SS]Nd?~t-ExE/pN}ojPV@X[]1_0Cz}X({Sc%r@`bl6E:Th<a' );
define( 'LOGGED_IN_KEY',    '}4#1`(~QeQRfG-mn21U7kGWYtYC?7[:OJ2qIHdX<#6Nw{6etUB;wPjGhrwvx*s<U' );
define( 'NONCE_KEY',        'Rk8>Ii9sw78h;4jcqs:Lh`v`9aTI/x|`p#([4NO6lV{+6R -3R--dWmh^DpCMTJL' );
define( 'AUTH_SALT',        '^>I=3z7Y~1}NQu<BCoMcyXGP=1W),K]_h@]TT|el4w+1Cx(;B/hH|(r-87LzU],*' );
define( 'SECURE_AUTH_SALT', '9odwy5j.HwW|lAl#ZS)of>VT _rD&^E>|9xc|fga[l.8]3`y*yy9bJz]WAYYV oX' );
define( 'LOGGED_IN_SALT',   'Esn,lxCI9[=ryO`KnDH;4m yNL5;<X6h+ahr{5bkW!A#l=U=|*cMDk._c d_`Dze' );
define( 'NONCE_SALT',       's-Q(wEMb4m_%kJB.xj52E#jb*:X$S`v7`je/j%A~]$oDtaK^`}0sDUGtH`oTv%;<' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
